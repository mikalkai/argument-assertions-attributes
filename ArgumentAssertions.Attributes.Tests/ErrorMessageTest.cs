﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading;
using ArgumentAssertions.Attributes.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class ErrorMessageTest
    {
        #region Test Data

        public class A
        {
            [IsMandatory]
            public string Text { get; set; }
        }

        public class B
        {
            [IsMandatory(ErrorMessage = "Custom error message.")]
            public string Text { get; set; }
        }

        public class C
        {
            [IsMandatory(ResXType = typeof(Localization), ResXName = nameof(Localization.TextCustomErrorMessage))]
            public string Text { get; set; }
        }

        #endregion

        [TestCase]
        public void ErrorMessage_ErrorMessageIsNotSet_ValidationResultShouldHaveDefaultErrorMessage()
        {
            // Arrange
            var a = new A
            {
                Text = null
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Text' should not be null.") &&
                    result.MemberNames.Contains("Text"));
        }

        [TestCase]
        public void ErrorMessage_ErrorMessageIsSet_ValidationResultShouldHaveCustomErrorMessage()
        {
            // Arrange
            var b = new B
            {
                Text = null
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Custom error message.") &&
                    result.MemberNames.Contains("Text"));
        }

        [TestCase]
        public void ErrorMessage_ResxTypeAndResxNameAreSet_ValidationResultShouldHaveResourceErrorMessage()
        {
            // Arrange
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            var c = new C
            {
                Text = null
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Custom error message from resource.") &&
                    result.MemberNames.Contains("Text"));
        }

        [TestCase]
        public void ErrorMessage_ResxTypeAndResxNameAreSetAndCultureIsFI_ValidationResultShouldHaveResourceFinnishErrorMessage()
        {
            // Arrange
            Thread.CurrentThread.CurrentCulture = new CultureInfo("fi-FI");
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            var c = new C
            {
                Text = null
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Virheviesti resurssitiedostosta.") &&
                    result.MemberNames.Contains("Text"));
        }
    }
}