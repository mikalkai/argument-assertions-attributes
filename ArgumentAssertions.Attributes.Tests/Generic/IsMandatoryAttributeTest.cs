﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class IsMandatoryAttributeTest
    {
        #region Test Data

        public class A
        {
            [IsMandatory]
            public string Text { get; set; }
        }

        public class B
        {
            [IsMandatory]
            public int Number { get; set; }
        }

        public class C
        {
            [IsMandatory]
            public int? Nullable { get; set; }

            [IsMandatory]
            public IList List { get; set; }
        }

        public class D
        {
            [IsMandatory]
            public DateTime? StartTime { get; set; }

            [IsMandatory(WhenProperty = nameof(IsTemporary), HasValue = true)]
            public DateTime? EndTime { get; set; }

            public bool IsTemporary { get; set; }
        }

        public class E
        {
            [IsMandatory(ErrorMessage = "Custom error message.")]
            public string Text { get; set; }
        }

        #endregion

        [TestCase]
        public void IsMandatory_BbIsMandatoryAndCcIsMandatory_ErrorsShouldBeEmpty()
        {
            // Arrange
            var a = new A
            {
                Text = "not-null"
            };

            // Act
            var validation = a.Validate();

            // Result
            validation.Errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsMandatory_BbIsNull_ErrorsShouldHaveOneValidationResult()
        {
            // Arrange
            var a = new A
            {
                Text = null
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Text' should not be null.") &&
                    result.MemberNames.Contains("Text"));
        }

        [TestCase]
        public void IsMandatory_NumberIsNotSupported_ThrowTypeNotSupportedException()
        {
            // Arrange
            var b = new B
            {
                Number = 1
            };

            // Act
            Action act = () => b.Validate();

            // Result
            act.ShouldThrow<TypeNotSupportedException>()
                .Where(exception => exception.Message.Equals(
                    "Value type 'System.Int32' in property 'B.Number' is not supported with 'IsMandatoryAttribute'."));
        }

        [TestCase]
        public void IsMandatory_NullableAndInterfaceAreSupported_DoNotThrowTypeNotSupportedException()
        {
            // Arrange
            var c = new C
            {
                Nullable = 1,
                List = new ArrayList()
            };

            // Act
            Action act = () => c.Validate();

            // Result
            act.ShouldNotThrow<TypeNotSupportedException>();
        }

        [TestCase]
        public void IsMandatory_StartTimeIsNull_ErrorsShouldContainOneValidationResult()
        {
            // Arrange
            var d = new D
            {
                StartTime = null,
                EndTime = DateTime.Now,
                IsTemporary = false
            };

            // Act
            var errors = d.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'D.StartTime' should not be null.") &&
                    result.MemberNames.Contains("StartTime"));
        }

        [TestCase]
        public void IsMandatory_EndTimeIsNullAndIsTemporaryIsFalse_ErrorsShouldBeEmpty()
        {
            // Arrange
            var d = new D
            {
                StartTime = DateTime.Now,
                EndTime = null,
                IsTemporary = false
            };

            // Act
            var errors = d.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsMandatory_EndTimeIsNullAndIsTemporaryIsTrue_ErrorsShouldContainOneValidationResult()
        {
            // Arrange
            var d = new D
            {
                StartTime = DateTime.Now,
                EndTime = null,
                IsTemporary = true
            };

            // Act
            var errors = d.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'D.EndTime' should not be null.") &&
                    result.MemberNames.Contains("EndTime"));
        }

        [TestCase]
        public void IsMandatory_ErrorMessageIsSet_ErrorsShouldContainSetErrorMessage()
        {
            // Arrange
            var e = new E
            {
                Text = null
            };

            // Act
            var errors = e.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Custom error message.") &&
                    result.MemberNames.Contains("Text"));
        }
    }
}