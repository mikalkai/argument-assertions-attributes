﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class ValidateSingleTest
    {
        #region Test Data

        public class A
        {
            [IsMandatory]
            public B Bb { get; set; }

            [IsMandatory]
            public string Text { get; set; }
        }

        public class B
        {
            [IsMandatory]
            public C Cc { get; set; }

            [IsMandatory]
            public int? Number { get; set; }
        }

        public class C
        {
            [IsMandatory]
            public DateTime? Time { get; set; }
        }

        public class X
        {
            [IsMandatory]
            public Y Yy { get; set; }
        }

        public class Y
        {
            [IsMandatory]
            public X Xx { get; set; }
        }

        #endregion

        [TestCase]
        public void Validate_BbAndTextAreNull_ErrorsHaveTwoValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = null,
                Text = null
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(2);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb' should not be null.") &&
                    result.MemberNames.Contains("Bb"));
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Text' should not be null.") &&
                    result.MemberNames.Contains("Text"));
        }

        [TestCase]
        public void Validate_CcAndNumberAreNull_ErrorsHaveTwoValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Cc = null,
                    Number = null
                },
                Text = "not-null"
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(2);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Cc' should not be null.") &&
                    result.MemberNames.Contains("Bb.Cc"));
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Number' should not be null.") &&
                    result.MemberNames.Contains("Bb.Number"));
        }

        [TestCase]
        public void Validate_TimeIsNull_ErrorsHaveOneValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Cc = new C
                    {
                        Time = null
                    },
                    Number = 1
                },
                Text = "not-null"
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Cc.Time' should not be null.") &&
                    result.MemberNames.Contains("Bb.Cc.Time"));
        }

        [TestCase]
        public void Validate_YyIsNull_ErrorsHaveOneValidationResults()
        {
            // Arrange
            var x = new X
            {
                Yy = new Y
                {
                    Xx = new X
                    {
                        Yy = null
                    }
                }
            };

            // Act
            var errors = x.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'X.Yy.Xx.Yy' should not be null.") &&
                    result.MemberNames.Contains("Yy.Xx.Yy"));
        }
    }
}