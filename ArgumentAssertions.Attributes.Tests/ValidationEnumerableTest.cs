﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class ValidationEnumerableTest
    {
        #region Test Data

        public class A
        {
            public B Bb { get; set; }
        }

        public class B
        {
            [IsMandatory]
            public List<C> Items { get; set; }
        }

        public class C
        {
            [IsMandatory]
            public int? Number { get; set; }
        }

        #endregion

        [TestCase]
        public void Validate_ItemsIsNull_ErrorsHaveOneValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Items = null
                }
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Items' should not be null.") &&
                    result.MemberNames.Contains("Bb.Items"));
        }

        [TestCase]
        public void Validate_TimeIsNullInAllItems_ErrorsHaveThreeValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Items = new List<C>
                    {
                        new C {Number = null},
                        new C {Number = null},
                        new C {Number = null},
                    }
                }
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(3);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Items[0].Number' should not be null.") &&
                    result.MemberNames.Contains("Bb.Items[0].Number"));
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Items[1].Number' should not be null.") &&
                    result.MemberNames.Contains("Bb.Items[1].Number"));
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Items[2].Number' should not be null.") &&
                    result.MemberNames.Contains("Bb.Items[2].Number"));
        }
    }
}