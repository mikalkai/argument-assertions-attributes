﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class PropertyPathTest
    {
        [TestCase(null)]
        [TestCase("")]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void Ctor_PropertyPathIsNullOrEmpty_ThrowArgumentException(string propertyPath)
        {
            // Arrange

            // Act
            Action act = () =>
            {
                new PropertyPath(propertyPath);
            };

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.ParamName.Equals("propertyPath"));
        }

        [TestCase("A")]
        [TestCase("A.Bb")]
        [TestCase("A.Bb.Cc")]
        public void AsString_PropertyPathIsNotEmpty_ResultShouldBeSameAsCtorArgument(string propertyPath)
        {
            // Arrange
            var path = new PropertyPath(propertyPath);

            // Act
            var result = path.AsString;

            // Result
            result.Should().Be(propertyPath);
        }

        [TestCase("A", 1, "A", "A")]
        [TestCase("A.Bb", 2, "A", "Bb")]
        [TestCase("A.Bb.Cc", 3, "A", "Cc")]
        public void AsLinkedList_PropertyPathIsNotEmpty_ResultShouldBeInSameOrderThanCtorArgument(string propertyPath, int count, string first, string last)
        {
            // Arrange
            var path = new PropertyPath(propertyPath);

            // Act
            var result = path.AsLinkedList;

            // Result
            result.Should().HaveCount(count);
            result.First.Value.Should().Be(first);
            result.Last.Value.Should().Be(last);
        }

        [TestCase("A", "A")]
        [TestCase("A.Bb", "A")]
        [TestCase("A.Bb.Cc", "A.Bb")]
        public void SkipLast_PropertyPathIsNotEmpty_ResultShouldNotHaveLastProperty(string propertyPath, string expected)
        {
            // Arrange
            var path = new PropertyPath(propertyPath);

            // Act
            var result = path.SkipLast;

            // Result
            result.Should().Be(expected);
        }

        [TestCase("A", "A")]
        [TestCase("A.Bb", "Bb")]
        [TestCase("A.Bb.Cc", "Bb.Cc")]
        public void SkipFirst_PropertyPathIsNotEmpty_ResultShouldNotHaveFirstProperty(string propertyPath, string expected)
        {
            // Arrange
            var path = new PropertyPath(propertyPath);

            // Act
            var result = path.SkipFirst;

            // Result
            result.Should().Be(expected);
        }
    }
}