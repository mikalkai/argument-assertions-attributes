﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class IsMatchAttributeTest
    {
        #region Test Data

        public class A
        {
            [IsMatch("abc")]
            public string Text { get; set; }
        }

        public class B
        {
            [IsMatch(null)]
            public string Text { get; set; }
        }

        public class C
        {
            [IsMatch("")]
            public string Text { get; set; }
        }

        #endregion

        [TestCase("a", 1)]
        [TestCase("ab", 1)]
        [TestCase("abc", 0)]
        [TestCase("abcd", 0)]
        public void IsMatch_PatternIsNotEmpty_TextShouldMatchWithPattern(string text, int errorCount)
        {
            // Arrange
            var a = new A
            {
                Text = text
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(errorCount);
            if (errorCount == 1)
            {
                errors.Should().Contain(
                    result =>
                        result.ErrorMessage.Equals("Parameter 'A.Text' should match with pattern 'abc' when comparison options are 'None'.") &&
                        result.MemberNames.Contains("Text"));
            }
        }

        [TestCase]
        public void IsMatch_PatternIsNotEmpty_NullShouldNotCauseValidationError()
        {
            // Arrange
            var a = new A
            {
                Text = null
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(0);
        }

        [TestCase]
        public void IsMatch_PatternIsNull_ThrowArgumentException()
        {
            // Arrange
            var b = new B
            {
                Text = "abc"
            };

            // Act
            Action act = () => b.Validate();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.ParamName.Equals("pattern"));
        }

        [TestCase]
        public void IsMatch_PatternIsEmpty_ThrowArgumentException()
        {
            // Arrange
            var b = new B
            {
                Text = "abc"
            };

            // Act
            Action act = () => b.Validate();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.ParamName.Equals("pattern"));
        }
    }
}