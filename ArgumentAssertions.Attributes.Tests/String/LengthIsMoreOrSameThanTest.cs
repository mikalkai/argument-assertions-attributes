﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Attributes.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class LengthIsMoreOrSameThanTest
    {
        #region Test Data

        public class A
        {
            [LengthIsMoreOrSameThan(6)]
            public List<string> List { get; set; }
        }

        public class B
        {
            [LengthIsMoreOrSameThan(6)]
            public string String { get; set; }
        }

        #endregion

        [Test]
        public void LengthIsMoreOrSame_BindPropertyIsList_ThrowTypeNotSupportedException()
        {
            // Arrange
            var a = new A
            {
                List = new List<string>()
            };

            // Act
            Action act = () => a.Validate();

            // Result
            act.ShouldThrow<TypeNotSupportedException>();
        }

        [Test]
        public void LengthIsMoreOrSame_BindPropertyIsString_DoNotThrowTypeNotSupportedException()
        {
            // Arrange
            var b = new B
            {
                String = ""
            };

            // Act
            Action act = () => b.Validate();

            // Result
            act.ShouldNotThrow<TypeNotSupportedException>();
        }
    }
}