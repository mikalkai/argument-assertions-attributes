﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using ArgumentAssertions.Attributes.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class StartsWithAttributeTest
    {
        #region Test Data

        public class A
        {
            [StartsWith("abc")]
            public string Text { get; set; }
        }

        public class B
        {
            [StartsWith("abc", Options = RegexOptions.IgnoreCase)]
            public string Text { get; set; }
        }

        #endregion

        [TestCase("a", 1)]
        [TestCase("ab", 1)]
        [TestCase("aabcd", 1)]
        [TestCase("ababcd", 1)]
        [TestCase("Abc", 1)]
        [TestCase("ABC", 1)]
        [TestCase("abc", 0)]
        [TestCase("abcd", 0)]
        [TestCase("abcde", 0)]
        public void StartsWith_PatternIsNotEmptyAndOptionsAreNone_TextShouldStartWithPattern(string text, int errorCount)
        {
            // Arrange
            var a = new A
            {
                Text = text
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(errorCount);
            if (errorCount == 1)
            {
                errors.Should().Contain(
                    result =>
                        result.ErrorMessage.Equals("Parameter 'A.Text' should start with 'abc' when comparison options are 'None'.") &&
                        result.MemberNames.Contains("Text"));
            }
        }

        [TestCase("a", 1)]
        [TestCase("ab", 1)]
        [TestCase("aabcd", 1)]
        [TestCase("ababcd", 1)]
        [TestCase("Abc", 0)]
        [TestCase("ABC", 0)]
        [TestCase("abc", 0)]
        [TestCase("abcd", 0)]
        [TestCase("abcde", 0)]
        public void StartsWith_PatternIsNotEmptyAndOptionsAreIgnoreCase_TextShouldStartWithPattern(string text, int errorCount)
        {
            // Arrange
            var b = new B
            {
                Text = text
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(errorCount);
            if (errorCount == 1)
            {
                errors.Should().Contain(
                    result =>
                        result.ErrorMessage.Equals("Parameter 'B.Text' should start with 'abc' when comparison options are 'IgnoreCase'.") &&
                        result.MemberNames.Contains("Text"));
            }
        }
    }
}