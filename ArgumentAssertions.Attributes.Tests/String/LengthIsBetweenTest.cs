﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Attributes.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class LengthIsBetweenTest
    {
        #region Test Data

        public class A
        {
            [LengthIsBetween(1, 6)]
            public List<string> List { get; set; }
        }

        public class B
        {
            [LengthIsBetween(1, 6)]
            public string String { get; set; }
        }

        #endregion

        [Test]
        public void LengthIsBetween_BindPropertyIsList_ThrowTypeNotSupportedException()
        {
            // Arrange
            var a = new A
            {
                List = new List<string>()
            };

            // Act
            Action act = () => a.Validate();

            // Result
            act.ShouldThrow<TypeNotSupportedException>();
        }

        [Test]
        public void LengthIsBetween_BindPropertyIsString_DoNotThrowTypeNotSupportedException()
        {
            // Arrange
            var b = new B
            {
                String = ""
            };

            // Act
            Action act = () => b.Validate();

            // Result
            act.ShouldNotThrow<TypeNotSupportedException>();
        }
    }
}