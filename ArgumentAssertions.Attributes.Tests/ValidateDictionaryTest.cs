﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class ValidateDictionaryTest
    {
        public class A
        {
            public B Bb { get; set; }
        }

        public class B
        {
            [IsMandatory]
            public Dictionary<string, C> Map { get; set; }
        }

        public class C
        {
            [IsMandatory]
            public string Text { get; set; }
        }

        [TestCase]
        public void Validate_MapIsNull_ErrorsHaveOneValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Map = null
                }
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Map' should not be null.") &&
                    result.MemberNames.Contains("Bb.Map"));
        }

        [TestCase]
        public void Validate_TextIsNullInAllItems_ErrorsHaveThreeValidationResults()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Map = new Dictionary<string, C>
                    {
                        {"a", new C {Text = null} },
                        {"b", new C {Text = null} },
                        {"c", new C {Text = null} },
                    }
                }
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(3);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Map[a].Text' should not be null.") &&
                    result.MemberNames.Contains("Bb.Map[a].Text"));
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Map[b].Text' should not be null.") &&
                    result.MemberNames.Contains("Bb.Map[b].Text"));
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Bb.Map[b].Text' should not be null.") &&
                    result.MemberNames.Contains("Bb.Map[b].Text"));
        }
    }
}