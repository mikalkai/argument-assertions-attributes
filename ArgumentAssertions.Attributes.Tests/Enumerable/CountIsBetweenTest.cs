﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Enumerable;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.Enumerable
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class CountIsBetweenTest
    {
        #region Test Data

        public class A
        {
            [CountIsBetween(2, 4)]
            public List<string> List { get; set; }
        }

        public class B
        {
            [CountIsBetween(-1, 4)]
            public List<string> List { get; set; }
        }

        public class C
        {
            [CountIsBetween(2, 0)]
            public List<string> List { get; set; }
        }

        #endregion

        [TestCase(1, "1")]
        [TestCase(0, "1", "2")]
        [TestCase(0, "1", "2", "3")]
        [TestCase(0, "1", "2", "3", "4")]
        [TestCase(1, "1", "2", "3", "4", "5")]
        public void CountIsBetween_ListCountIsOrIsNotBetweenLowAndHigh_ErrorsShouldHaveOneValidationResultWhenNotBetween(
            int errorCount, 
            params string[] array)
        {
            // Arrange
            var a = new A
            {
                List = array.ToList()
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(errorCount);
            if (errorCount == 1)
            {
                errors.Should().Contain(
                    result =>
                        result.ErrorMessage.Equals("Parameter 'A.List' should have count between '2' and '4'.") &&
                        result.MemberNames.Contains("List"));
            }
        }

        [TestCase]
        public void CountIsBetween_LowIsNegative_ThrowArgumentException()
        {
            // Arrange
            var b = new B
            {
                List = new List<string>()
            };

            // Act
            Action act = () => b.Validate();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.ParamName.Equals("low"));
        }

        [TestCase]
        public void CountIsBetween_HighIsLessThanLow_ThrowArgumentException()
        {
            // Arrange
            var c = new C
            {
                List = new List<string>()
            };

            // Act
            Action act = () => c.Validate();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.ParamName.Equals("high"));
        }
    }
}