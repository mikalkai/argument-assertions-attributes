﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Enumerable;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.Enumerable
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class CountIsMoreOrSameThanAttributeTest
    {
        #region Test Data

        public class A
        {
            [CountIsMoreOrSameThan(4)]
            public List<string> List { get; set; }
        }

        public class B
        {
            [CountIsMoreOrSameThan(Property = nameof(Count))]
            public List<string> List { get; set; }

            public int Count { get; set; }
        }

        public class C
        {
            [CountIsMoreOrSameThan(Property = nameof(OtherList))]
            public List<string> List { get; set; }

            public List<string> OtherList { get; set; }
        }

        public class D
        {
            [CountIsMoreOrSameThan(Property = "NotFound")]
            public List<string> List { get; set; }
        }

        public class E
        {
            [CountIsMoreOrSameThan(Property = nameof(NotSupported))]
            public List<string> List { get; set; }

            public bool NotSupported { get; set; }
        }

        public class F
        {
            [CountIsMoreOrSameThan(4, ErrorMessage = "Custom error message.")]
            public List<string> List { get; set; }
        }

        #endregion

        [TestCase("a", "b", "c", "d")]
        [TestCase("a", "b", "c", "d", "e")]
        public void CountIsMoreOrSameThanValue_ListHasMoreOrSameThanFourItems_ErrorsShouldBeEmpty(params string[] array)
        {
            // Arrange
            var a = new A
            {
                List = array.ToList()
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void CountIsMoreOrSameThanValue_ListIsNull_ErrorsShouldBeEmpty()
        {
            // Arrange
            var a = new A
            {
                List = null
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase("a", "b", "c")]
        public void CountIsMoreOrSameThanValue_ListHasLessThanFourItems_ErrorsShouldHaveOneValidationResult(params string[] array)
        {
            // Arrange
            var a = new A
            {
                List = array.ToList()
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.List' should have count more or same than '4'.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void CountIsLessThanValue_ErrorMessageIsSet_ErrorShouldContainSetErrorMessage()
        {
            // Arrange
            var f = new F
            {
                List = new List<string> {"1", "2", "3"},
            };

            // Act
            var errors = f.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Custom error message.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase("a", "b", "c", "d")]
        [TestCase("a", "b", "c", "d", "e")]
        public void CountIsMoreOrSameThanProperty_ListCountIsMoreOrSameThanCount_ErrorsShouldBeEmpty(params string[] array)
        {
            // Arrange
            var b = new B
            {
                List = array.ToList(),
                Count = 4
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void CountIsMoreOrSameThanProperty_ListIsNull_ErrorsShouldBeEmpty()
        {
            // Arrange
            var b = new B
            {
                List = null
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase("a", "b", "c")]
        public void CountIsMoreOrSameThanProperty_ListCountIsLessThanCount_ErrorsShouldHaveOneValidationResult(params string[] array)
        {
            // Arrange
            var b = new B
            {
                List = array.ToList(),
                Count = 4
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'B.List' should have count more or same than property 'B.Count'.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase("a", "b", "c", "d")]
        [TestCase("a", "b", "c", "d", "e")]
        public void CountIsMoreOrSameThanProperty_ListCountIsMoreOrSameThanOtherListCount_ErrorsShouldBeEmpty(params string[] array)
        {
            // Arrange
            var c = new C
            {
                List = array.ToList(),
                OtherList = new List<string> { "e", "f", "g", "h" },
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase("a", "b", "c")]
        public void CountIsMoreOrSameThanProperty_ListCountIsLessThanOtherListCount_ErrorsShouldHaveOneValidationResult(params string[] array)
        {
            // Arrange
            var c = new C
            {
                List = array.ToList(),
                OtherList = new List<string> { "e", "f", "g", "h" }
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'C.List' should have count more or same than property 'C.OtherList'.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void CountIsMoreOrSameThanProperty_PropertyIsNotFound_ThrowPropertyNotFoundException()
        {
            // Arrange
            var d = new D
            {
                List = new List<string>()
            };

            // Act
            Action act = () => d.Validate();

            // Result
            act.ShouldThrow<PropertyNotFoundException>()
                .WithMessage($"Could not find property with name 'NotFound' from object '{typeof(D).FullName}'.");
        }

        [TestCase]
        public void CountIsMoreOrSameThanProperty_PropertyIsNotSupported_ThrowPropertyTypeNotSupportedException()
        {
            // Arrange
            var e = new E
            {
                List = new List<string>(),
                NotSupported = true
            };

            // Act
            Action act = () => e.Validate();

            // Result
            act.ShouldThrow<PropertyTypeNotSupportedException>()
                .WithMessage($"Property 'NotSupported' with type 'System.Boolean' is not supported in '{typeof(CountIsMoreOrSameThanAttribute).Name}'.");
        }
    }
}