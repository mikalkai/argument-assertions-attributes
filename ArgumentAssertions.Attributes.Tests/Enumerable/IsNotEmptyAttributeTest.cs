﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Enumerable;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.Enumerable
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class IsNotEmptyAttributeTest
    {
        #region Test Data

        public class A
        {
            [IsNotEmpty]
            public string Text { get; set; }
        }

        public class B
        {
            [IsNotEmpty]
            public List<int> List { get; set; }
        }

        public class C
        {
            [IsNotEmpty]
            public Dictionary<string, DateTime?> Map { get; set; }
        }

        public class D
        {
            [IsNotEmpty(ErrorMessage = "Custom error message.")]
            public string Text { get; set; }
        }

        #endregion

        [TestCase("not empty")]
        [TestCase(null)]
        public void IsNotEmpty_TextIsNullOrNotEmpty_ErrorsShouldBeEmpty(string text)
        {
            // Arrange
            var a = new A
            {
                Text = text
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsNotEmpty_TextIsEmpty_ErrorsShouldHaveOneValidationResult()
        {
            // Arrange
            var a = new A
            {
                Text = ""
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.Text' should not be empty.") &&
                    result.MemberNames.Contains("Text"));
        }

        [TestCase]
        public void IsNotEmpty_ListIsNull_ErrorsShouldBeEmpty()
        {
            // Arrange
            var b = new B
            {
                List = null
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsNotEmpty_ListIsNotEmpty_ErrorsShouldBeEmpty()
        {
            // Arrange
            var b = new B
            {
                List = new List<int> {1, 2, 3}
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsNotEmpty_ListIsEmpty_ErrorsShouldHaveOneValidationResult()
        {
            // Arrange
            var b = new B
            {
                List = new List<int>()
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'B.List' should not be empty.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void IsNotEmpty_MapIsNull_ErrorsShouldBeEmpty()
        {
            // Arrange
            var c = new C
            {
                Map = null
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsNotEmpty_MapIsNotEmpty_ErrorsShouldBeEmpty()
        {
            // Arrange
            var c = new C
            {
                Map = new Dictionary<string, DateTime?>
                {
                    {"a", DateTime.Now},
                    {"b", null},
                    {"c", DateTime.Now},
                }
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void IsNotEmpty_MapIsEmpty_ErrorsShouldHaveOneValidationResult()
        {
            // Arrange
            var c = new C
            {
                Map = new Dictionary<string, DateTime?>()
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'C.Map' should not be empty.") &&
                    result.MemberNames.Contains("Map"));
        }

        [TestCase]
        public void IsNotEmpty_ErrorMessageIsSet_ErrorsShouldContainSetErrorMessage()
        {
            // Arrange
            var d = new D
            {
                Text = ""
            };

            // Act
            var errors = d.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Custom error message.") &&
                    result.MemberNames.Contains("Text"));
        }
    }
}