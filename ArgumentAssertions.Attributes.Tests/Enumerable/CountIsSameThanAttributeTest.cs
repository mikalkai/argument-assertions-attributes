﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ArgumentAssertions.Attributes.Enumerable;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Attributes.Tests.Enumerable
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class CountIsSameThanAttributeTest
    {
        #region Test Data

        public class A
        {
            [CountIsSameThan(4)]
            public List<string> List { get; set; }
        }

        public class B
        {
            [CountIsSameThan(Property = nameof(Count))]
            public List<string> List { get; set; }

            public int Count { get; set; }
        }

        public class C
        {
            [CountIsSameThan(Property = nameof(OtherList))]
            public List<string> List { get; set; }

            public List<string> OtherList { get; set; }
        }

        public class D
        {
            [CountIsSameThan(Property = "NotFound")]
            public List<string> List { get; set; }
        }

        public class E
        {
            [CountIsSameThan(Property = nameof(NotSupported))]
            public List<string> List { get; set; }

            public bool NotSupported { get; set; }
        }

        public class F
        {
            [CountIsSameThan(4, ErrorMessage = "Custom error message.")]
            public List<string> List { get; set; }
        }

        #endregion

        [TestCase]
        public void CountIsSameThanValue_ListHasFourItems_ErrorsShouldBeEmpty()
        {
            // Arrange
            var a = new A
            {
                List = new List<string> {"a", "b", "c", "d"}
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void CountIsSameThanValue_ListIsNull_ErrorsShouldBeEmpty()
        {
            // Arrange
            var a = new A
            {
                List = null
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase("a", "b", "c")]
        [TestCase("a", "b", "c", "d", "e")]
        public void CountIsSameThanValue_ListDoesNotHaveFourItems_ErrorsShouldHaveOneValidationResult(params string[] array)
        {
            // Arrange
            var a = new A
            {
                List = array.ToList()
            };

            // Act
            var errors = a.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'A.List' should have count '4'.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void CountIsSameThanValue_ErrorMessageIsSet_ErrorShouldContainSetErrorMessage()
        {
            // Arrange
            var f = new F
            {
                List = new List<string> {"1", "2", "3"},
            };

            // Act
            var errors = f.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Custom error message.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void CountIsSameThanProperty_ListCountIsSameThanCount_ErrorsShouldBeEmpty()
        {
            // Arrange
            var b = new B
            {
                List = new List<string> { "a", "b", "c", "d" },
                Count = 4
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase]
        public void CountIsSameThanProperty_ListIsNull_ErrorsShouldBeEmpty()
        {
            // Arrange
            var b = new B
            {
                List = null
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase("a", "b", "c")]
        [TestCase("a", "b", "c", "d", "e")]
        public void CountIsSameThanProperty_ListCountIsNotSameThanCount_ErrorsShouldHaveOneValidationResult(params string[] array)
        {
            // Arrange
            var b = new B
            {
                List = array.ToList(),
                Count = 4
            };

            // Act
            var errors = b.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'B.List' should have same count than property 'B.Count'.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void CountIsSameThanProperty_ListCountIsSameThanOtherListCount_ErrorsShouldBeEmpty()
        {
            // Arrange
            var c = new C
            {
                List = new List<string> { "a", "b", "c", "d" },
                OtherList = new List<string> { "e", "f", "g", "h" },
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().BeEmpty();
        }

        [TestCase("a", "b", "c")]
        [TestCase("a", "b", "c", "d", "e")]
        public void CountIsSameThanProperty_ListCountIsNotSameThanOtherListCount_ErrorsShouldHaveOneValidationResult(params string[] array)
        {
            // Arrange
            var c = new C
            {
                List = array.ToList(),
                OtherList = new List<string> { "e", "f", "g", "h" }
            };

            // Act
            var errors = c.Validate().Errors.ToList();

            // Result
            errors.Should().HaveCount(1);
            errors.Should().Contain(
                result =>
                    result.ErrorMessage.Equals("Parameter 'C.List' should have same count than property 'C.OtherList'.") &&
                    result.MemberNames.Contains("List"));
        }

        [TestCase]
        public void CountIsSameThanProperty_PropertyIsNotFound_ThrowPropertyNotFoundException()
        {
            // Arrange
            var d = new D
            {
                List = new List<string>()
            };

            // Act
            Action act = () => d.Validate();

            // Result
            act.ShouldThrow<PropertyNotFoundException>()
                .WithMessage($"Could not find property with name 'NotFound' from object '{typeof(D).FullName}'.");
        }

        [TestCase]
        public void CountIsSameThanProperty_PropertyIsNotSupported_ThrowPropertyTypeNotSupportedException()
        {
            // Arrange
            var e = new E
            {
                List = new List<string>(),
                NotSupported = true
            };

            // Act
            Action act = () => e.Validate();

            // Result
            act.ShouldThrow<PropertyTypeNotSupportedException>()
                .WithMessage($"Property 'NotSupported' with type 'System.Boolean' is not supported in '{typeof(CountIsSameThanAttribute).Name}'.");
        }
    }
}