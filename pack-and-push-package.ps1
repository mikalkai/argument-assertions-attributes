#
# Description:
#	This powershell script will pack and publish a new version from Argument Assertions package
#	to the nuget.org gallery.
#
# Pre-requisites:
#	Use nuget setapikey 00000000-0000-0000-0000-00000000 to configure your api key.
#	You can get your own api key from nuget.org under account information.
#

# Parameters
Param (
	[switch]$push
)

# 1. Clean old packages
del *.nupkg

# 2. Build and pack new packages
nuget.exe pack ".\ArgumentAssertions.Attributes\ArgumentAssertions.Attributes.csproj" -Build -MSBuildVersion 14 -Symbols -Properties Configuration=Release

# 3. Push
if ($push) {
	$nupkg = Get-ChildItem "." -Filter *.nupkg | Where-Object {$_.FullName -inotlike "*symbols*"}
	nuget push $nupkg
}
