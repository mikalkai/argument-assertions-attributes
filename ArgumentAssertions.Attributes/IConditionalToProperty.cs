﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;

namespace ArgumentAssertions.Attributes
{
    /// <summary>
    /// Mark attribute validation conditional.
    /// </summary>
    public interface IConditionalToProperty
    {
        /// <summary>
        /// Get or set conditionally affected property.
        /// </summary>
        string WhenProperty { get; set; }

        /// <summary>
        /// Get or set value when the condiotionality is valid.
        /// </summary>
        object HasValue { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class IConditionalToPropertyExtensions
    {
        /// <summary>
        /// Check if the property has the specified value in place.
        /// </summary>
        /// <param name="conditional"></param>
        /// <param name="parent">Parent object.</param>
        /// <returns>True if property has the specified value. Otherwise false.</returns>
        public static bool WhenPropertyHasValue(this IConditionalToProperty conditional, object parent)
        {
            conditional.ThrowIf(() => conditional).Null();
            conditional.WhenProperty.ThrowIf(() => conditional.WhenProperty).Empty();

            // Property is defined, find the property
            var propertyInfo = parent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .FirstOrDefault(info => info.Name.Equals(conditional.WhenProperty, StringComparison.OrdinalIgnoreCase));
            if (propertyInfo == null)
            {
                throw new PropertyNotFoundException(conditional.WhenProperty, parent);
            }

            // If property value is null, check if the specified value is also null
            var value = propertyInfo.GetValue(parent);
            if (value == null)
            {
                return conditional.HasValue == null;
            }

            // Compare values
            return value.Equals(conditional.HasValue);
        }
    }
}