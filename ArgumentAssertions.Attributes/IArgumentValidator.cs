using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ArgumentAssertions.Attributes
{
    /// <summary>
    /// Marker interface for a validation result.
    /// </summary>
    public interface IArgumentValidator
    {
        /// <summary>
        /// Value validation errors.
        /// </summary>
        ICollection<ValidationResult> Errors { get; }
    }
}