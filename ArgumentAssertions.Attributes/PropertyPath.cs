﻿using System.Collections.Generic;
using System.Linq;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;

namespace ArgumentAssertions.Attributes
{
    public class PropertyPath
    {
        public PropertyPath(string propertyPath)
        {
            AsString = propertyPath.ThrowIf(nameof(propertyPath)).Empty();
        }

        /// <summary>
        /// Get property path value.
        /// </summary>
        public string AsString { get; }

        /// <summary>
        /// Get property path as linked list.
        /// </summary>
        public LinkedList<string> AsLinkedList => new LinkedList<string>(AsString.Split('.'));

        /// <summary>
        /// Get property path but skip last property.
        /// <remarks>
        /// If there is only one property full path is returned.
        /// </remarks>
        /// </summary>
        public string SkipLast
        {
            get
            {
                var parts = AsString.Split('.');
                return parts.Length > 1
                    ? $"{string.Join(".", parts.Take(parts.Length - 1))}"
                    : AsString;
            }
        }

        /// <summary>
        /// Get property path but skip first property.
        /// <remarks>
        /// If there is only one property full path is returned.
        /// </remarks>
        /// </summary>
        public string SkipFirst
        {
            get
            {
                var parts = AsString.Split('.');
                return parts.Length > 1
                    ? $"{string.Join(".", parts.Skip(1))}"
                    : AsString;
            }
        }

        public static implicit operator string(PropertyPath propertyPath)
        {
            return propertyPath.AsString;
        }

        public static implicit operator PropertyPath(string propertyPath)
        {
            return new PropertyPath(propertyPath);
        }

        public override string ToString()
        {
            return AsString;
        }
    }
}