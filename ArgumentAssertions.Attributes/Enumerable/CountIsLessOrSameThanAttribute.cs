﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ArgumentAssertions.Attributes.Enumerable.Internal;

namespace ArgumentAssertions.Attributes.Enumerable
{
    /// <summary>
    /// Validate that <see cref="IEnumerable" /> count is less or same than specified value or bind property.
    /// <remarks>
    /// Applicable on:
    ///     Any type extending <see cref="IEnumerable"/>. 
    /// Supported types for property:
    ///     <see cref="int"/>, 
    ///     <see cref="long"/>, 
    ///     <see cref="IEnumerable"/>.
    /// </remarks>
    /// </summary>
    public class CountIsLessOrSameThanAttribute : CountIsAttribute
    {
        public CountIsLessOrSameThanAttribute() {}

        public CountIsLessOrSameThanAttribute(int value)
            : base(value) {}

        protected override bool IsValid(int count, int expected)
        {
            return count <= expected;
        }

        protected override bool IsValid(int count, long expected)
        {
            return count <= expected;
        }

        protected override bool IsValid(int count, IEnumerable expected)
        {
            return count <= expected.Count();
        }

        protected override ValidationResult FailureOnValue(PropertyPath propertyPath)
        {
            return new ValidationResult(ResolveErrorMessage(
                $"Parameter '{propertyPath}' should have {QuantityName} less or same than '{Value}'."),
                    new List<string> {propertyPath.SkipFirst});
        }

        protected override ValidationResult FailureOnProperty(PropertyPath propertyPath)
        {
            return new ValidationResult(ResolveErrorMessage(
                $"Parameter '{propertyPath}' should have {QuantityName} less or same than property '{this.GetPathToProperty(propertyPath)}'."),
                    new List<string> {propertyPath.SkipFirst});
        }
    }
}