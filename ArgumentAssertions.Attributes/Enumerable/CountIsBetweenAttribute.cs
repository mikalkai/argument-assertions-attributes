﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ArgumentAssertions.Attributes.Enumerable.Internal;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;

namespace ArgumentAssertions.Attributes.Enumerable
{
    /// <summary>
    /// Validate that <see cref="IEnumerable" /> count is between the specified range.
    /// <remarks>
    /// Applicable on:
    ///     Any type extending <see cref="IEnumerable"/>. 
    /// </remarks>
    /// </summary>
    public class CountIsBetweenAttribute : ArgumentValidatorAttribute
    {
        public CountIsBetweenAttribute(int low, int high)
        {
            _low = low.ThrowIf(nameof(low)).Negative();
            _high = high.ThrowIf(nameof(high)).LessThan(low);

            SupportedOnType<IEnumerable>();
        }

        private readonly int _high;
        private readonly int _low;

        protected virtual string QuantityName => "count";

        protected override ValidationResult Validate(object value, object parent, PropertyPath propertyPath)
        {
            // If value is not set, return success
            if (value == null)
            {
                return ValidationResult.Success;
            }

            // Value if not null, validate
            return CountIsBetween(value) ? ValidationResult.Success : Failure(propertyPath);
        }

        private bool CountIsBetween(object value)
        {
            return InScope<IEnumerable>(value, enumerable =>
            {
                var count = enumerable.Count();
                return count >= _low && count <= _high;
            });
        }

        private ValidationResult Failure(PropertyPath propertyPath)
        {
            return new ValidationResult(ResolveErrorMessage(
                $"Parameter '{propertyPath}' should have {QuantityName} between '{_low}' and '{_high}'."),
                    new List<string> {propertyPath.SkipFirst});
        }
    }
}