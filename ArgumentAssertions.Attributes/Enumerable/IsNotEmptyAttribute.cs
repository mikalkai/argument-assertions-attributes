﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ArgumentAssertions.Attributes.Enumerable.Internal;

namespace ArgumentAssertions.Attributes.Enumerable
{
    /// <summary>
    /// Is not empty attribute checks if the underlying property is not empty.
    /// <remarks>
    /// Applicable on:
    ///     Any type extending <see cref="IEnumerable"/>.
    /// </remarks>
    /// </summary>
    public class IsNotEmptyAttribute : ArgumentValidatorAttribute
    {
        public IsNotEmptyAttribute()
        {
            SupportedOnType<IEnumerable>();
        }

        protected override ValidationResult Validate(object value, object parent, PropertyPath propertyPath)
        {
            // If value is not set, return success
            if (value == null)
            {
                return ValidationResult.Success;
            }

            // Value if not null, validate
            return IsNotEmpty(value) ? ValidationResult.Success : Failure(propertyPath);
        }

        private bool IsNotEmpty(object value)
        {
            return InScope<IEnumerable>(value, enumerable => enumerable.Count() > 0);
        }

        private ValidationResult Failure(PropertyPath propertyPath)
        {
            return new ValidationResult(ResolveErrorMessage($"Parameter '{propertyPath}' should not be empty."),
                new List<string> {propertyPath.SkipFirst});
        }
    }
}