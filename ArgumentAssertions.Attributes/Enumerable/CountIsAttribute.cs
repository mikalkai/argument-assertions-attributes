﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using ArgumentAssertions.Attributes.Enumerable.Internal;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;

namespace ArgumentAssertions.Attributes.Enumerable
{
    public abstract class CountIsAttribute : ArgumentValidatorAttribute, IComparableToProperty
    {
        protected CountIsAttribute()
            : this(0) {}

        protected CountIsAttribute(int value)
        {
            Value = value.ThrowIf(nameof(value)).Negative();

            SupportedOnType<IEnumerable>();
        }

        public string Property { get; set; }

        protected int Value { get; set; }

        protected virtual string QuantityName => "count";

        protected override ValidationResult Validate(object value, object parent, PropertyPath propertyPath)
        {
            // If value is not set, return success
            if (value == null)
            {
                return ValidationResult.Success;
            }

            // Value is not null, validate
            if (Property != null)
            {
                return CountIsSameThanProperty(value, parent) ? ValidationResult.Success : FailureOnProperty(propertyPath);
            }
            return CountIsSameThanValue(value) ? ValidationResult.Success : FailureOnValue(propertyPath);
        }

        /// <summary>
        /// Check if enumerable count is valid against <see cref="int"/> type.
        /// </summary>
        /// <param name="count">Enumerable count.</param>
        /// <param name="expected">Expected value.</param>
        /// <returns>True if valid. Otherwise false.</returns>
        protected abstract bool IsValid(int count, int expected);

        /// <summary>
        /// Check if enumerable count is valid against <see cref="long"/> type.
        /// </summary>
        /// <param name="count">Enumerable count.</param>
        /// <param name="expected">Expected value.</param>
        /// <returns>True if valid. Otherwise false.</returns>
        protected abstract bool IsValid(int count, long expected);

        /// <summary>
        /// Check if enumerable count is valid against <see cref="IEnumerable"/> type.
        /// </summary>
        /// <param name="count">Enumerable count.</param>
        /// <param name="expected">Expected value.</param>
        /// <returns>True if valid. Otherwise false.</returns>
        protected abstract bool IsValid(int count, IEnumerable expected);

        /// <summary>
        /// Create validation result when failure occurs on value.
        /// </summary>
        /// <param name="propertyPath">Property path in failed property.</param>
        /// <returns>Validation result.</returns>
        protected abstract ValidationResult FailureOnValue(PropertyPath propertyPath);

        /// <summary>
        /// Create validation result when failure occurs on property.
        /// </summary>
        /// <param name="propertyPath">Property path in failed property.</param>
        /// <returns>Validation result.</returns>
        protected abstract ValidationResult FailureOnProperty(PropertyPath propertyPath);

        private bool CountIsSameThanValue(object value)
        {
            return InScope<IEnumerable>(value, enumerable => IsValid(enumerable.Count(), Value));
        }

        private bool CountIsSameThanProperty(object value, object parent)
        {
            return InScope<IEnumerable>(value, enumerable =>
            {
                // Find property value where to match
                var thanProperty = this.GetThanProperty(parent);
                if (thanProperty == null)
                {
                    throw new PropertyNotFoundException(Property, parent);
                }

                var count = enumerable.Count();

                // Match against int
                if (thanProperty is int)
                {
                    return IsValid(count, (int) thanProperty);
                }

                // Match against long
                if (thanProperty is long)
                {
                    return IsValid(count, (long) thanProperty);
                }

                // Match against other enumerable
                var otherEnumerable = thanProperty as IEnumerable;
                if (otherEnumerable != null)
                {
                    return IsValid(count, (IEnumerable) thanProperty);
                }

                // Property type is not supported, throw exception
                throw new PropertyTypeNotSupportedException(Property, thanProperty.GetType(), this);
            });
        }
    }
}