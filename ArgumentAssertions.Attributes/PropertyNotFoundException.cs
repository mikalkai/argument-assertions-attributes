﻿using System;

namespace ArgumentAssertions.Attributes
{
    public class PropertyNotFoundException : Exception
    {
        public PropertyNotFoundException(string propertyName, object parent)
            : base($"Could not find property with name '{propertyName}' from object '{parent.GetType().FullName}'.") {}
    }
}