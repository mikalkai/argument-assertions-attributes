using System;

namespace ArgumentAssertions.Attributes
{
    [Flags]
    public enum SupportedOn
    {
        /// <summary>
        /// Supported on any type.
        /// </summary>
        Any = 0,

        /// <summary>
        /// Supported with a type that is a class.
        /// </summary>
        Class = 1,

        /// <summary>
        /// Supported with a type that is an interface.
        /// </summary>
        Interface = 2,

        /// <summary>
        /// Supported with any nullable type.
        /// </summary>
        Nullable = 4,

        /// <summary>
        /// Supported on specific types.
        /// </summary>
        Specific = 8
    }
}