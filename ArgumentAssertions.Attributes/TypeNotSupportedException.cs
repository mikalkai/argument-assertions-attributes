﻿using System;

namespace ArgumentAssertions.Attributes
{
    public class TypeNotSupportedException : Exception
    {
        public TypeNotSupportedException(string propertyPath, string attributeName, Type valueType)
            : base($"Value type '{valueType.FullName}' in property '{propertyPath}' is not supported with '{attributeName}'.") {}
    }
}