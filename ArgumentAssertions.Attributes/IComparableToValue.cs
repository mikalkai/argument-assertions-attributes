﻿namespace ArgumentAssertions.Attributes
{
    public interface IComparableToValue<T>
    {
         T ThanValue { get; set; }
    }
}