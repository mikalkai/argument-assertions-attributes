﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ArgumentAssertions.Attributes
{
    /// <summary>
    /// Mark attribute comparable to other property.
    /// </summary>
    public interface IComparableToProperty
    {
        /// <summary>
        /// Set or get compared property name.
        /// </summary>
        string Property { get; set; } 
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class IComparableToPropertyExtensions
    {
        /// <summary>
        /// Find ThanProperty object from the parent scope.
        /// </summary>
        /// <param name="comparable"></param>
        /// <param name="parent">Parent object.</param>
        /// <returns>Object bind with ThanProperty or null if one was not found.</returns>
        [SuppressMessage("ReSharper", "UseIsOperator.2")]
        [SuppressMessage("ReSharper", "LoopCanBeConvertedToQuery")]
        public static object GetThanProperty(this IComparableToProperty comparable, object parent)
        {
            foreach (var propertyInfo in parent.GetType().GetProperties().Where(info => info.CanRead))
            {
                if (propertyInfo.Name.Equals(comparable.Property, StringComparison.OrdinalIgnoreCase))
                {
                    return propertyInfo.GetValue(parent);
                }
            }
            return null;
        }

        /// <summary>
        /// Value path ThanProperty by using either the included property path or display name included in context.
        /// </summary>
        /// <param name="comparable"></param>
        /// <param name="propertyPath">Path to property being validated.</param>
        /// <returns></returns>
        public static string GetPathToProperty(this IComparableToProperty comparable, string propertyPath)
        {
            var parts = propertyPath.Split('.');
            return $"{string.Join(".", parts.Take(parts.Length - 1))}.{comparable.Property}";
        }
    }
}