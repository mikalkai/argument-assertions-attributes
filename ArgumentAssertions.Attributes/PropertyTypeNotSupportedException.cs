﻿using System;

namespace ArgumentAssertions.Attributes
{
    public class PropertyTypeNotSupportedException : Exception
    {
        public PropertyTypeNotSupportedException(string propertyName, Type propertyType, ArgumentValidatorAttribute attribute)
            : base($"Property '{propertyName}' with type '{propertyType.FullName}' is not supported in '{attribute.GetType().Name}'.") {}
    }
}