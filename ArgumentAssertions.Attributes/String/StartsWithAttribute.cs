﻿namespace ArgumentAssertions.Attributes.String
{
    public class StartsWithAttribute : IsMatchAttribute
    {
        public StartsWithAttribute(string pattern)
            : base($"^{pattern}")
        {
            _pattern = pattern;
        }

        private readonly string _pattern;

        protected override string DefaultErrorMessage(PropertyPath propertyPath)
        {
            return $"Parameter '{propertyPath}' should start with '{_pattern}' when comparison options are '{Options}'.";
        }
    }
}