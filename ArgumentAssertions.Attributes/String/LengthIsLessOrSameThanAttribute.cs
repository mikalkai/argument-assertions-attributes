﻿using System.Collections;
using ArgumentAssertions.Attributes.Enumerable;

namespace ArgumentAssertions.Attributes.String
{
    /// <summary>
    /// Validate that <see cref="string" /> count is less or same than specified value or bind property.
    /// <remarks>
    /// Applicable on:
    ///     <see cref="string"/>. 
    /// Supported types for property:
    ///     <see cref="int"/>, 
    ///     <see cref="long"/>, 
    ///     <see cref="IEnumerable"/>.
    /// </remarks>
    /// </summary>
    public class LengthIsLessOrSameThanAttribute : CountIsLessOrSameThanAttribute
    {
        public LengthIsLessOrSameThanAttribute()
            : this(0) {}

        public LengthIsLessOrSameThanAttribute(int value)
            : base(value)
        {
            ClearSupportedTypes();
            SupportedOnType<string>();
        }

        protected override string QuantityName => "length";
    }
}