﻿using ArgumentAssertions.Attributes.Enumerable;

namespace ArgumentAssertions.Attributes.String
{
    /// <summary>
    /// Validate that <see cref="string" /> count is less or same than specified value or bind property.
    /// <remarks>
    /// Applicable on:
    ///     <see cref="string"/>. 
    /// </remarks>
    /// </summary>
    public class LengthIsBetweenAttribute : CountIsBetweenAttribute
    {
        public LengthIsBetweenAttribute(int low, int high)
            : base(low, high)
        {
            ClearSupportedTypes();
            SupportedOnType<string>();
        }

        protected override string QuantityName => "length";
    }
}