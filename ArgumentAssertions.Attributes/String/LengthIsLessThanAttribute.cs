﻿using System.Collections;
using ArgumentAssertions.Attributes.Enumerable;

namespace ArgumentAssertions.Attributes.String
{
    /// <summary>
    /// Validate that <see cref="string" /> count is less than specified value or bind property.
    /// <remarks>
    /// Applicable on:
    ///     <see cref="string"/>. 
    /// Supported types for property:
    ///     <see cref="int"/>, 
    ///     <see cref="long"/>, 
    ///     <see cref="IEnumerable"/>.
    /// </remarks>
    /// </summary>
    public class LengthIsLessThanAttribute : CountIsLessThanAttribute
    {
        public LengthIsLessThanAttribute()
            : this(0) {}

        public LengthIsLessThanAttribute(int value)
            : base(value)
        {
            ClearSupportedTypes();
            SupportedOnType<string>();
        }

        protected override string QuantityName => "length";
    }
}