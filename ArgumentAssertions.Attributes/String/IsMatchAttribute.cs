﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;

namespace ArgumentAssertions.Attributes.String
{
    /// <summary>
    /// Validate that <see cref="string" /> count is less or same than specified value or bind property.
    /// <remarks>
    /// Applicable on:
    ///     <see cref="string"/>. 
    /// </remarks>
    /// </summary>
    public class IsMatchAttribute : ArgumentValidatorAttribute
    {
        public IsMatchAttribute(string pattern)
        {
            Pattern = pattern.ThrowIf(nameof(pattern)).Empty();

            SupportedOnType<string>();
        }

        protected readonly string Pattern;

        /// <summary>
        /// Get or set match options.
        /// </summary>
        public RegexOptions Options { get; set; }

        protected override ValidationResult Validate(object value, object parent, PropertyPath propertyPath)
        {
            // If value is not set, return success
            if (value == null)
            {
                return ValidationResult.Success;
            }

            return IsMatch(value) ? ValidationResult.Success : Failure(propertyPath);
        }

        /// <summary>
        /// Return the default error message.
        /// </summary>
        /// <param name="propertyPath">Current property path.</param>
        /// <returns>Error message.</returns>
        protected virtual string DefaultErrorMessage(PropertyPath propertyPath)
        {
            return $"Parameter '{propertyPath}' should match with pattern '{Pattern}' when comparison options are '{Options}'.";
        }

        private bool IsMatch(object value)
        {
            return InScope<string>(value, s => Regex.IsMatch(s, Pattern, Options));
        }

        private ValidationResult Failure(PropertyPath propertyPath)
        {
            return new ValidationResult(ResolveErrorMessage(DefaultErrorMessage(propertyPath)), 
                new List<string> {propertyPath.SkipFirst});
        }
    }
}