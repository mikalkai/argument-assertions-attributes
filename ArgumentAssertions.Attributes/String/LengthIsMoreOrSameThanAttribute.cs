﻿using System.Collections;
using ArgumentAssertions.Attributes.Enumerable;

namespace ArgumentAssertions.Attributes.String
{
    /// <summary>
    /// Validate that <see cref="string" /> count is more or same than specified value or bind property.
    /// <remarks>
    /// Applicable on:
    ///     <see cref="string"/>. 
    /// Supported types for property:
    ///     <see cref="int"/>, 
    ///     <see cref="long"/>, 
    ///     <see cref="IEnumerable"/>.
    /// </remarks>
    /// </summary>
    public class LengthIsMoreOrSameThanAttribute : CountIsMoreOrSameThanAttribute
    {
        public LengthIsMoreOrSameThanAttribute()
            : this(0) {}

        public LengthIsMoreOrSameThanAttribute(int value)
            : base(value)
        {
            ClearSupportedTypes();
            SupportedOnType<string>();
        }

        protected override string QuantityName => "length";
    }
}