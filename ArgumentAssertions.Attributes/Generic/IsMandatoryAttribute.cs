﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ArgumentAssertions.Attributes.Generic
{
    /// <summary>
    /// Marks any property as required and mandatory by validating that it is not null.
    /// <remarks>
    /// Applicable on:
    ///     Class,
    ///     Interface,
    ///     Nullable&lt;T&gt;.
    /// Supported types for property:
    ///     <see cref="bool"/>, 
    ///     <see cref="byte"/>, 
    ///     <see cref="char"/>, 
    ///     <see cref="double"/>, 
    ///     <see cref="float"/>, 
    ///     <see cref="int"/>, 
    ///     <see cref="long"/>, 
    ///     <see cref="short"/>,  
    ///     Enum.
    /// </remarks>
    /// </summary>
    public class IsMandatoryAttribute : ArgumentValidatorAttribute, IConditionalToProperty
    {
        public IsMandatoryAttribute()
        {
            SupportedOn = SupportedOn.Class | SupportedOn.Interface | SupportedOn.Nullable;
        }

        public string WhenProperty { get; set; }

        public object HasValue { get; set; }

        protected override ValidationResult Validate(object value, object parent, PropertyPath propertyPath)
        {
            // Conditional property is not set, check always
            if (WhenProperty == null)
            {
                return IsNotNull(value) ? ValidationResult.Success : Failure(propertyPath);
            }

            // Conditional property is set and the property has the defined value, check if value is null
            if (!this.WhenPropertyHasValue(parent))
            {
                return ValidationResult.Success;
            }
            return IsNotNull(value) ? ValidationResult.Success : Failure(propertyPath);
        }

        private bool IsNotNull(object value)
        {
            return value != null;
        }

        private ValidationResult Failure(PropertyPath propertyPath)
        {
            return new ValidationResult(ResolveErrorMessage($"Parameter '{propertyPath}' should not be null."),
                new List<string> {propertyPath.SkipFirst});
        }
    }
}