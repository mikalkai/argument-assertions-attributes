﻿using System;

namespace ArgumentAssertions.Attributes
{
    public class TypeCastException : Exception
    {
        public TypeCastException(object value, Type excpectedType)
            : base($"Cannot cast value with type {value.GetType().FullName} to expected type {excpectedType.FullName}.") {}
    }
}