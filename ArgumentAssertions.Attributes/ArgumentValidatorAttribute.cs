﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace ArgumentAssertions.Attributes
{
    /// <summary>
    /// Base class for all fluent IsAttributes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public abstract class ArgumentValidatorAttribute : Attribute
    {
        private readonly List<Type> _supportedOnTypes = new List<Type>();

        /// <summary>
        /// Get or set pre-defined error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Get or set resourse file type.
        /// </summary>
        public Type ResXType { get; set; }

        /// <summary>
        /// Get or set resource name.
        /// </summary>
        public string ResXName { get; set; }

        /// <summary>
        ///   Value or set supported types flag.
        /// </summary>
        protected SupportedOn SupportedOn { get; set; }

        /// <summary>
        /// Validate property value bind with the attribute.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">Value to be validated.</param>
        /// <param name="parent">Parent object.</param>
        /// <param name="propertyPath">Current property path.</param>
        /// <returns><see cref="ValidationResult.Success" /> if no errors. Otherwise ValidationResult contains error details.</returns>
        public ValidationResult Validate(Type valueType, object value, object parent, PropertyPath propertyPath)
        {
            ThrowIfNotSupportedOn(valueType, propertyPath);
            return Validate(value, parent, propertyPath);
        }

        /// <summary>
        /// Handle validation on extending class.
        /// </summary>
        /// <param name="value">Value to be validated.</param>
        /// <param name="parent">Parent object.</param>
        /// <param name="propertyPath">Current property path.</param>
        /// <returns><see cref="ValidationResult.Success" /> if no errors. Otherwise ValidationResult contains error details.</returns>
        protected abstract ValidationResult Validate(object value, object parent, PropertyPath propertyPath);

        /// <summary>
        /// Add supported type.
        /// </summary>
        /// <typeparam name="T">Type of supported object.</typeparam>
        protected void SupportedOnType<T>()
        {
            SupportedOn = SupportedOn | SupportedOn.Specific;

            _supportedOnTypes.Add(typeof (T));
        }

        /// <summary>
        /// Clear all supported types.
        /// </summary>
        protected void ClearSupportedTypes()
        {
            SupportedOn = SupportedOn.Any;

            _supportedOnTypes.Clear();
        }

        /// <summary>
        /// Safely cast and validate object.
        /// </summary>
        /// <typeparam name="T">Expected type.</typeparam>
        /// <param name="value">Value to be validated.</param>
        /// <param name="validate">Validation function.</param>
        /// <returns>True if validation succeeds.</returns>
        protected bool InScope<T>(object value, Func<T, bool> validate)
        {
            // ReSharper disable once UseIsOperator.2
            if (value != null && !typeof (T).IsInstanceOfType(value))
            {
                throw new TypeCastException(value, typeof (T));
            }
            return validate((T) value);
        }

        /// <summary>
        /// Resolve used error message.
        /// Error message is resolved in the following way:
        /// 1. If <c>ErrorMessage</c> is set it will be always used. 
        /// 2. If <c>ResxType</c> and <c>ResxName</c> are set error message is loaded from resource using the current UI culture. 
        /// 3. Otherwise the <c>defaultErrorMessage</c> is used. 
        /// </summary>
        /// <returns></returns>
        protected string ResolveErrorMessage(string defaultErrorMessage)
        {
            if (ErrorMessage != null)
            {
                return ErrorMessage;
            }

            if (ResXType != null && ResXName != null)
            {
                var resourceManager = new ResourceManager(ResXType);
                return resourceManager.GetString(ResXName);
            }

            return defaultErrorMessage;
        }

        /// <summary>
        /// Check if the attribute is supported in the current value.
        /// </summary>
        /// <param name="valueType">Type of the value being validated.</param>
        /// <param name="propertyPath">Current property path.</param>
        /// <exception cref="TypeNotSupportedException"></exception>
        private void ThrowIfNotSupportedOn(Type valueType, PropertyPath propertyPath)
        {
            if (SupportedOn == SupportedOn.Any)
            {
                return;
            }

            if (SupportedOn.HasFlag(SupportedOn.Class) && valueType.IsClass)
            {
                return;
            }

            if (SupportedOn.HasFlag(SupportedOn.Interface) && valueType.IsInterface)
            {
                return;
            }

            if (SupportedOn.HasFlag(SupportedOn.Nullable) && Nullable.GetUnderlyingType(valueType) != null)
            {
                return;
            }

            if (SupportedOn.HasFlag(SupportedOn.Specific) &&
                _supportedOnTypes.Any(supportedOnType => supportedOnType.IsAssignableFrom(valueType)))
            {
                return;
            }

            throw new TypeNotSupportedException(propertyPath, GetType().Name, valueType);
        }
    }
}