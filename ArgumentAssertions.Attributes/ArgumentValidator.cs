﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ArgumentAssertions.Attributes
{
    public static class ArgumentValidator
    {
        /// <summary>
        /// Validate any type.
        /// </summary>
        /// <typeparam name="T">Type of validated object.</typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static IArgumentValidator Validate<T>(this T t)
        {
            return new ArgumentValidator<T>(t);
        }

        /// <summary>
        /// Throw <see cref="ValidationException"/> in case validation contains errors.
        /// </summary>
        /// <param name="argumentValidator"></param>
        public static void AndThrow(this IArgumentValidator argumentValidator)
        {
            if (argumentValidator.Errors.Count > 0)
            {
                throw new ValidationException();
            }
        }
    }

    public class ArgumentValidator<T> : IArgumentValidator
    {
        public ArgumentValidator(T value)
        {
            if (Equals(value, default(T)))
            {
                throw new ArgumentNullException(nameof(value), $"Parameter '{value}' should not be null.");
            }

            ValidateProperties(value, value.GetType().Name);
        }

        public ICollection<ValidationResult> Errors { get; } = new List<ValidationResult>();

        private void ValidateProperties(object parent, string parentPath)
        {
            foreach (var propertyInfo in parent.GetType().GetProperties().Where(info => info.CanRead))
            {
                var property = propertyInfo.GetValue(parent);
                var propertyType = propertyInfo.PropertyType;
                var propertyPath = $"{parentPath}.{propertyInfo.Name}";

                var attributes = propertyInfo.GetCustomAttributes(typeof (ArgumentValidatorAttribute), false).Cast<ArgumentValidatorAttribute>().ToList();
                attributes.ForEach(attribute =>
                {
                    var dictionary = property as IDictionary;
                    if (dictionary != null)
                    {
                        ValidateDictionary(parent, attribute, propertyType, dictionary, propertyPath);
                        return;
                    }

                    var s = property as string;
                    if (s != null)
                    {
                        ValidateSingle(parent, attribute, propertyType, property, propertyPath);
                        return;
                    }

                    var enumerable = property as IEnumerable;
                    if (enumerable != null)
                    {
                        ValidateEnumerable(parent, attribute, propertyType, enumerable, propertyPath);
                        return;
                    }

                    ValidateSingle(parent, attribute, propertyType, property, propertyPath);
                });

                if (!StepIn(property))
                {
                    continue;
                }

                ValidateProperties(property, propertyPath);
            }
        }

        private static bool StepIn(object property)
        {
            if (property == null)
            {
                return false;
            }

            if (property is IEnumerable)
            {
                return false;
            }

            if (property.GetType().IsValueType)
            {
                return false;
            }

            return true;
        }

        [SuppressMessage("ReSharper", "LoopCanBePartlyConvertedToQuery")]
        private void ValidateEnumerable(object parent, ArgumentValidatorAttribute attribute, Type propertyType, IEnumerable enumerable, string propertyPath)
        {
            ValidateSingle(parent, attribute, propertyType, enumerable, propertyPath);

            var index = 0;
            foreach (var item in enumerable)
            {
                if (!StepIn(item))
                {
                    continue;
                }
                ValidateProperties(item, $"{propertyPath}[{index++}]");
            }
        }

        private void ValidateDictionary(object parent, ArgumentValidatorAttribute attribute, Type propertyType, IDictionary dictionary, string propertyPath)
        {
            ValidateSingle(parent, attribute, propertyType, dictionary, propertyPath);

            foreach (var key in dictionary.Keys)
            {
                var value = dictionary[key];
                if (!StepIn(value))
                {
                    continue;
                }
                ValidateProperties(value, $"{propertyPath}[{key}]");
            }
        }
        private void ValidateSingle(object parent, ArgumentValidatorAttribute attribute, Type propertyType, object property, string propertyPath)
        {
            var validationResult = attribute.Validate(propertyType, property, parent, propertyPath);
            if (validationResult != ValidationResult.Success)
            {
                Errors.Add(validationResult);
            }
        }
    }
}